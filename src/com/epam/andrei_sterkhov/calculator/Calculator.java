package com.epam.andrei_sterkhov.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Calculator {
    private List<Double> operands;
    private List<String> signs;
    private int currentSignIndex;
    private String expression;
    private Validator validator = new Validator();

    private void setExpression(String expr) {
        validator.checkExpression(expr);
        this.expression = expr;
    }

    public double getResult(String expr) {
        setExpression(expr);
        removeBrackets();
        return getTempResult(expression);
    }

    private double getTempResult(String string) {
        operands = getOperands(string);
        signs = getSigns(string);
        currentSignIndex = 0;
        // If operands has only one element, return it
        if (operands.size() == 1) {
            return operands.get(0);
        }

        removeMultiplyAndDivide();
        removeMinusAndPlus();
        return operands.get(0);
    }

    private void removeMultiplyAndDivide() {
        // Ищем в списке знаки * или / и применяем их
        double result;
        boolean hasMultiplyAndDivideSigsns = true;
        while (hasMultiplyAndDivideSigsns) {
            getMultiplyAndDivideSignIndex();
            if (currentSignIndex != -1) {
                result = getSimpleResult(getFirst(), getSecond(), getCurrentSign());
                applyResult(result);
            } else {
                hasMultiplyAndDivideSigsns = false;
            }
        }
    }

    private void removeMinusAndPlus() {
        currentSignIndex = 0;
        double result;
        while (!signs.isEmpty()) {
            result = getSimpleResult(getFirst(), getSecond(), getCurrentSign());
            applyResult(result);
        }
    }

    // Return list opernands from expression
    private List<Double> getOperands(String string) {
        // Получаем список операндов
        List<String> stringOperands = new ArrayList<>(Arrays.asList(string.split("[+/*\\-]")));

        // Если первый элемент пустой (если в начале выражения стоит "-"), то делаем его равным 0
        if (stringOperands.get(0).equals("")) {
            stringOperands.remove(0);
            stringOperands.add(0, "0");
        }
        // Переводим список строк в список double
        return stringOperands.stream()
                .map(Double::parseDouble)
                .collect(Collectors.toList());
    }

    // Return list signs from expression
    private List<String> getSigns(String string) {
        // Получаем список знаков
        List<String> tempSigns = new ArrayList<>(Arrays.asList(string.split("\\d+[.]?\\d*")));

        // Если в начале исходной строки нет "-" то первый элемент будет пустым
        // Удаляем первый элемент, проверив не пустой ли у нас список
        if (!tempSigns.isEmpty() && tempSigns.get(0).equals("")) {
            tempSigns.remove(0);
        }
        return tempSigns;
    }

    // Return result of simple expression like 4 + 5
    private double getSimpleResult(double first, double second, String sign) {
        double result = 0;
        switch (sign) {
            case "*":
                result = first * second;
                break;
            case "/":
                checkDivideZero(second);
                result = first / second;
                break;
            case "-":
                result = first - second;
                break;
            case "+":
                result = first + second;
                break;
            default:
        }
        return result;
    }

    private void checkDivideZero(double number) {
        if (Double.compare(0, number) == 0) {
            throw new ArithmeticException("Divide by zero");
        }
    }

    private void getMultiplyAndDivideSignIndex() {
        int multiIndex = signs.indexOf("*");
        // Если больше нет знака * , то обрабатываем /
        if (multiIndex == -1) {
            currentSignIndex = signs.indexOf("/");
        }
        int divideIndex = signs.indexOf("/");
        // Если больше нет знака / , то обрабатываем *
        if (divideIndex == -1) {
            currentSignIndex = signs.indexOf("*");
        }
        if (multiIndex != -1 && divideIndex != -1) {
            currentSignIndex = multiIndex > divideIndex ? divideIndex : multiIndex;
        }
    }

    private double getFirst() {
        return operands.get(currentSignIndex);
    }

    private double getSecond() {
        return operands.get(currentSignIndex + 1);
    }

    private String getCurrentSign() {
        return signs.get(currentSignIndex);
    }

    // Reduce our list of signs and operands
    private void applyResult(double resulst) {
        //Убираем знак из нашего листа
        signs.remove(currentSignIndex);
        // Убираем наши операнды из листа операндов и вставляем наш result
        operands.remove(currentSignIndex);
        operands.remove(currentSignIndex);
        operands.add(currentSignIndex, resulst);
    }

    private void removeBrackets() {
        boolean hasBrackets = true;
        while (hasBrackets) {
            // Ищем закрывающуюся скобку
            int closeIndex = expression.indexOf(')');
            if (closeIndex != -1) {
                // Ищем открывающую скобку перед нашей закрывающей
                int openIndex = expression.lastIndexOf('(', closeIndex);

                // Получаем первую часть нашей строки, исключая открывающую скобку
                String first = expression.substring(0, openIndex);

                // Получаем вторую часть нашей строки, исключая закрывающую скобку
                String second = expression.substring(closeIndex + 1, expression.length());

                // Получаем result выражения в скобках
                String tempExpression = this.expression.substring(openIndex + 1, closeIndex);
                double result = getTempResult(tempExpression);

                // Склеиваем наши строки
                this.expression = getReducedExpression(first, second, result);
            } else {
                hasBrackets = false;
            }
        }
    }

    private String getReducedExpression(String first, String second, double result) {
        //Обработать result, если он отрицательный
        if (result < 0 && first.length() != 0) {
            char ch = first.charAt(first.length() - 1);
            switch (ch) {
                //Если первая строка оканчивается на - то заменяем его на +
                case '-':
                    first = first.substring(0, first.length() - 1) + "+";
                    result = Math.abs(result);
                    break;
                //Если первая строка оканчивается на + убираем этот плюс
                case '+':
                    first = first.substring(0, first.length() - 1);
                    break;
                // Если первая строка оканчивается на * или /
                // то ищем последнее вхождение плюса или минуса и меняем его на противоположный знак
                case '*':
                case '/':
                    int plusIndex = first.lastIndexOf('+');
                    int minusIndex = first.lastIndexOf('-');
                    int index = plusIndex > minusIndex ? plusIndex : minusIndex;
                    if (index != -1) {
                        char signChar = (first.charAt(index) == '-') ? '+' : '-';
                        first = first.substring(0, index) + signChar + first.substring(index + 1);
                    } else {
                        first = "-" + first;
                    }
                    result = Math.abs(result);
                    break;

                default:
            }
        }
        return first + result + second;
    }
}