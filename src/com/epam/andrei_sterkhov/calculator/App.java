package com.epam.andrei_sterkhov.calculator;

public class App {
    public static void main(String[] args) {
        String[] arrStrWithoutBracket = {
                "-2/4",
                "-(2+(40/40)+3)*2",
                "(-4+8)",
                "0/(2",
                "3+5/((6-8)/4+(4-11/2)+2)",
                "5+2/6",
        };
        Calculator calculator = new Calculator();
        for (String str : arrStrWithoutBracket) {
            System.out.println(calculator.getResult(str));
        }
    }
}
